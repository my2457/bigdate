import Vue from 'vue'
import ElementUI from 'element-ui'
import router from './router'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App.vue'
import echarts from 'echarts'
import axios from 'axios'
Vue.prototype.$http = axios
Vue.prototype.$echarts = echarts 

Vue.use(ElementUI)

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})