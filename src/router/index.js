import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Side from '@/components/views/side'
import Footer from '@/components/views/footer'
import Sale from '@/components/views/charts/sales'
import Brand from '@/components/views/charts/brand'
import Price from '@/components/views/charts/price'
import NoPrice from '@/components/views/charts/no_price'
import Vioce from '@/components/views/charts/vioce'
import Opinions from '@/components/views/charts/opinions'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Footer
    },
    {
      path: '/sale',
      component: Sale
    },
    {
      path: '/brand',
      component: Brand
    },
    {
      path: '/price',
      component: Price
    },
    {
      path: '/no_price',
      component: NoPrice
    },
    {
      path: '/vioce',
      component: Vioce
    },
    {
      path: '/opinions',
      component: Opinions
    }
  ]
})
